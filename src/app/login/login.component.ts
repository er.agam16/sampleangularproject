import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../authentication.service';
import {Router} from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import {Constants} from '../shared/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: object;

  constructor(private authService: AuthenticationService, private router: Router, private lStorage: LocalStorageService) {
    this.user = {
      'email': 'agam',
      'password': ''
    };
    console.log('i am in user constructor');
  }

  ngOnInit() {
  }

  handleLogin() {
    console.log(this.user);
    this.authService.login(this.user['email'], this.user['password']).subscribe((data) => {
      console.log(data);
      alert(' Hey, we got a successfull response');
      this.lStorage.add(Constants.USER_LOGGED_IN_KEY, true);
      this.router.navigate(['/']);
    }, (err) => {
      alert('There is some error in processing your request. Please try again.');
      console.log(err);
    });
  }
}
